/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.Users;
import controller.exampleSessionBean;
import javax.ws.rs.PathParam;

/**
 * REST Web Service
 *
 * @author jozi_
 */
@Path("users")
public class UsersResource {

    @Context
    private UriInfo context;

    @EJB // <-- important!!!!
    private exampleSessionBean esb; // <-- important!!!! <-- important!!!!

    /**
     * Creates a new instance of UsersResource
     */
    public UsersResource() {
    }

    /**
     * Retrieves representation of an instance of controller.UsersResource
     *
     * @return an instance of java.lang.String
     */
    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getUserJson(@PathParam("id") int id) {
        //TODO return proper representation object
        Users u = esb.readUserByID(id);
        // user found and returning json data about login priviliges and activity        

        JsonObjectBuilder job = Json.createObjectBuilder();
        job.add("id", u.getId());
        job.add("login", u.getUsername());
        job.add("password", u.getPassword());

        return job.build().toString();
    }

    /**
     * Retrieves representation of an instance of controller.UsersResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        List<Users> ulst = esb.readAllUsers();
        // user found and returning json data about login priviliges and activity        
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Users u : ulst) {
            JsonObjectBuilder job = Json.createObjectBuilder();
            job.add("id", u.getId());
            job.add("login", u.getUsername());
            job.add("password", u.getPassword());
            JsonObject userValue = job.build();
            builder.add(userValue);
        }
        JsonArray userA = builder.build();

        return userA.toString();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String addUser(@FormParam("username") String username, @FormParam("password") String password) {

        System.out.println(username); // TODO CHECK VALID INPUTS!
        System.out.println(password);

        Users u = new Users();
        u.setUsername(username);
        u.setPassword(password);
        Users nu = esb.insert(u);

        boolean succes = false;

        if (nu.getPassword() != null && nu.getPassword().equals(password)) {
            succes = true;
        }

        JsonObject jsono = Json.createObjectBuilder()
                .add("username", username)
                .add("password", password)
                .add("succes", succes)
                .build();

        return jsono.toString();
    }

    /**
     * PUT method for updating or creating an instance of UsersResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
