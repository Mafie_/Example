/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Users;

/**
 *
 * @author jozi_
 */
@Stateless
public class exampleSessionBean {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    @PersistenceContext
    private EntityManager em;

    // User
    public Users insert(Users u) {
        em.persist(u);
        return u;
    }

    public void update(Users u) {
        em.merge(u);
    }
    
    public List<Users> readAllUsers() {
        List<Users> ulst = em.createNamedQuery("Users.findAll").getResultList();
        if (ulst == null) {
            Users u = new Users(null); // <--- realy bad style of error prevention do not use if possible xD
            ulst.add(u);
            return ulst;
        } else {
            return ulst;
        }
    }
    
    public Users readUserByID(int id) {
        Users u = em.find(Users.class, id);
        if (u == null) {
            u = new Users(null);
            return u;
        } else {
            return u;
        }
    }
    
    public List<Users> readUserByUsername(String username) {
        List<Users> ul = em.createNamedQuery("Users.findByUsername").setParameter("username", username).getResultList();
        if (ul == null) {
            Users u = new Users(null);
            ul.add(u);
            return ul;
        } else {
            return ul;
        }
    }
    
}
